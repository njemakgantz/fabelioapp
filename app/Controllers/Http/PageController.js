'use strict'
const rp = require('request-promise');
const parser = require('node-html-parser')
const Page = use('App/Models/Page');
const Pricelist = use('App/Models/Pricelist');

class PageController {
    async home({view}) {
        return view.render('index')
    }

    async item({view,params}) {
        const page = await Page.find(params.id);
        const priceList = await Pricelist.query()
        .where('page_id',params.id)
        .fetch()
        console.log(priceList.toJSON())
        return view.render('item', { page: page.toJSON(), priceLists : priceList.toJSON() })
    }

    async submit({request, response, session}) {
        const { url } = request.all();
        try {
        let html = await rp(url)
        const root = parser.parse(html)
        let metadata = root.firstChild.querySelectorAll('meta')
        let datameta = {}
        for (let i = 0;i<metadata.length;i++){
            let stringmeta = metadata[i].rawAttrs
            if (stringmeta.includes('property')){
                stringmeta = stringmeta.replace(/"/gi, '')
                stringmeta = stringmeta.replace(/og:/gi, '')
                stringmeta = stringmeta.replace(/product:price:/gi, '')
                let key = stringmeta.substring(stringmeta.indexOf('property='), stringmeta.indexOf('content'))
                key = key.replace(/property=/gi, '')
                let content = stringmeta.substring(stringmeta.indexOf('content='), stringmeta.length)
                content = content.replace(/content=/gi, '')
                datameta[key.trim()] = content.trim()
            }
        }

        const page = new Page();
        
        page.fill(
            { name: datameta.title, 
              url: datameta.url,
              imageurl: datameta.image,
              description: datameta.description,
              price:datameta.amount
        })

        
            await page.save();
            return "Success"
        } catch (error) {
            session.flash({error: 'Not a valid fabelio product url'})
            return response.redirect('back')
        }
        
    }

    async list({view}) {
        const pages = await Page.all();
        return view.render('list', { pages: pages.toJSON() })
    }
}

module.exports = PageController
