'use strict'

const Task = use('Task')
const Page = use('App/Models/Page');
const Pricelist = use('App/Models/Pricelist');
const rp = require('request-promise');
const parser = require('node-html-parser')

class UpdatePrice extends Task {
  static get schedule () {
    return '0 * * * *'
  }

  async handle () {
    const pages = await Page.all()
    const pagesjson = pages.toJSON()
    for (let i = 0;i<pagesjson.length;i++){
        let pagenow = pagesjson[i]
        let html = await rp(pagenow.url)
        const root = parser.parse(html)
        let metadata = root.firstChild.querySelectorAll('meta')
        let datameta = {}
        for (let i = 0;i<metadata.length;i++){
            let stringmeta = metadata[i].rawAttrs
            if (stringmeta.includes('property')){
                stringmeta = stringmeta.replace(/"/gi, '')
                stringmeta = stringmeta.replace(/og:/gi, '')
                stringmeta = stringmeta.replace(/product:price:/gi, '')
                let key = stringmeta.substring(stringmeta.indexOf('property='), stringmeta.indexOf('content'))
                key = key.replace(/property=/gi, '')
                let content = stringmeta.substring(stringmeta.indexOf('content='), stringmeta.length)
                content = content.replace(/content=/gi, '')
                datameta[key.trim()] = content.trim()
            }
        }

        const priceList = new Pricelist();
        
        priceList.fill(
            { page_id: pagenow.id, 
              price: datameta.amount
        })


        try {
          await priceList.save()
        }catch (error) {

        }
        // console.log(datameta.amount)
    }
  }
}

module.exports = UpdatePrice
