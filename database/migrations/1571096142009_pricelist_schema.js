'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PricelistSchema extends Schema {
  up () {
    this.create('pricelists', (table) => {
      table.increments()
      table.integer('page_id')
      table.string('price', 255)
      table.timestamps()
    })
  }

  down () {
    this.drop('pricelists')
  }
}

module.exports = PricelistSchema
